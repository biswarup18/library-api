module.exports = app => {
 
  const ssfbook = require("../controllers/ssfbook.controller.js");  
  const statistics=require("../controllers/statistics.controller.js")
  var router = require("express").Router();

  router.get("/oldestbook",statistics.oldestbook);          // Find oldest book
  router.get("/newestbook",statistics.newestbook);          // Find newest book
  router.get("/heighstlentbook",statistics.heighstlentbook);      //Find heighst lent book
  router.get("/totallentbook",statistics.totallentbook);          //Find total lent book
  router.get("/totalbooks",statistics.totalbooks);               // Find total books
  router.get("/mostactiveuser",statistics.mostactiveuser);        //Find most active user
  router.get("/totaluser",statistics.totaluser);                  //Find total user
  router.get("/mostavailablebook",statistics.mostavailablebook);   
 
  
  // Fatch a list of all
  
  router.get("/book/:name", ssfbook.findAll);              //Fatch all book by name
  router.get("/books", ssfbook.filter);
  router.get("/book", ssfbook.sort);
 
  app.use("/api", router);
};
