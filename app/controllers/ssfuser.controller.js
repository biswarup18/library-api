const db = require("../models");
const User =db.user;
const Op = db.Sequelize.Op;
const Sequelize = require("sequelize");


// Retrieve all Books by subject from the database.
exports.findAll = (req, res) => {
    const username = req.params.username;
    var condition = username ? { username: { [Op.iLike]: `%${username}%` } } : null;
  
    User.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };
  

  exports.filter = (req, res) => {
    const roles = req.query.roles;
    var condition = roles ? { roles: { [Op.iLike]: `%${roles}%` } } : null;
  
    User.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };

  exports.sortname = (req, res) => {
   
    User.findAll({ where: {},
    order: [
      ['username','ASC']
      //['registration_date','DESC']
  ]
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };

  exports.sortrdate = (req, res) => {
   
    User.findAll({ where: {},
    order: [
     ['registration_date','DESC']
  ]
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };