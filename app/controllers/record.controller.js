const db = require("../models");
const Record = db.records;
const Book =db.books;
const Op = db.Sequelize.Op;

exports.checkbook = (req, res, next) => {
     // booked
    Record.findOne({
      where: {
        booked: req.body.booked
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          message: "Failed! Book is not available!"
        });
        return;
      }
      next();
    });
};

exports.create = (req, res) => {
  // Validate request
  if (!req.body.bookId) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }
    const records={
      bookId:req.body.bookId,
      userId:req.body.userId,
    issue_date: req.body.issue_date,
    submit_date: req.body.submit_date,
    booked: req.body.booked,        
  }
  Record.create(records)
    .then(data => {
      res.send(data);
      })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the record."
      });
    });
  };
   
// Retrieve all Records from the database.
exports.findAll = (req, res) => {
  Record.findAll({ where: {} })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Records."
      });
    });
};

// Find a single Record with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Record.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Record with id=" + id
      });
    });
};

// Update a Record by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Record.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Record was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Record with id=${id}. Maybe Record was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Record with id=" + id
      });
    });
};

// Delete a Record with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Record.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Record was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Record with id=${id}. Maybe Record was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Record with id=" + id
      });
    });
};

// Delete all Records from the database.
exports.deleteAll = (req, res) => {
  Record.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Records were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Records."
      });
    });
};