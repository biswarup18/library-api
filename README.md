****Full Article with implementation:****

**Node.js PostgreSql Library Management System API**

=> User can signup new account, or login with username & password.

=> User information will be stored in PostgreSQL database

=>By User’s role (librarian,student, faculty), authorize the User to access resources

**These are APIs :**

**Methods**    --      **Urls**	             --                       **Actions**

POST    	/api/auth/signup                             	 [signup new account]

POST        /api/auth/signin	                            [login an account]

GET     	/api/test/all	                                [retrieve public content(show all books)]

GET	        /api/test/student/:name	                        [access student’s content(find all book by name)]

GET	        /api/test/faculty	                            [access faculty’s content]

//librarian content

GET     	/api/test/librarian/ssfusers/:username		    [search user by name]

GET     	/api/test/librarian/ssfusers	                [(maintance)]

GET     	/api/test/librarian/ssfusersname	            [sort users by name(ASC)]

GET     	/api/test/librarian/ssfusersrdate	            [sort users by registration date(DESC)]

//books

POST    	/api/test/librarian/books	                	[create a book]

GET	        /api/test/librarian/books	                	[read all books]

GET	        /api/test/librarian/books/:id	            	[get book by id]

PUT	        /api/test/librarian/books/:id		            [update book by id]

DELETE  	/api/test/librarian/books/:id	                [delete book by id]

DELETE  	/api/test/librarian/books		                [delete all books]

//records

POST    	/api/test/librarian/records		                [create a record]

GET	        /api/test/librarian/records		                [find all records]

GET	        /api/test/librarian/records/:id		            [find a record by id]

PUT	        /api/test/librarian/records/:id		            [update a record by id]

DELETE  	/api/test/librarian/records/:id		            [delete a record by id]

DELETE  	/api/test/librarian/records		                [delete all record]

//payments

POST	    /api/test/librarian/payments		            [create a payment]

GET     	/api/test/librarian/payments		            [get all payments]

GET	        /api/test/librarian/payments/:id		        [get payments by id]

PUT	        /api/test/librarian/payments/:id		        [update payment by id]

DELETE	    /api/test/librarian/payments/:id		        [delete payment by id]

DELETE	    /api/test/librarian/payments		            [delete all payments]

//user

POST	    /api/test/librarian/users		                [create a user]

GET	        /api/test/librarian/users		                [get all users]

GET	        /api/test/librarian/users/:id		            [find a user by id]

PUT	        /api/test/librarian/users/:id		            [update user by id]

DELETE  	/api/test/librarian/users/:id		            [delete user by id]

DELETE  	/api/test/librarian/users		                [delete all users]

//all

GET		    /api/book/:name		                                [search book by name]

GET	    	/api/books?subject=enter subject name			    [filter book by subject]

GET	    	/api/book			                                [sort book by name and show no of copies available]

GET	        /api/oldestbook		                                [find oldest book in details]

GET     	/api/newestbook		                                [find newest book in details]

GET     	/api/heighstlentbook	                            [find heighest lent book id]

GET	        /api/totallentbook		                            [find total lent book]

GET	        /api/totalbooks		                                [find total book]

GET	        /api/mostactiveuser		                            [find most active user id]

GET	        /api/totaluser		                                [find total user]

GET	        /api/mostavailablebook	                            [find most available book id]


**Technology**

Express 4.17.1

bcryptjs 2.4.3

jsonwebtoken 8.5.1

Sequelize 5.21.3

PostgreSQL

**This is my project structure:**

![](a.png)  ![](b.png)

Let me explain it briefly.

– config

configure PostgreSQL database & Sequelize

configure Auth Key

– routes

auth.routes.js: POST signup & signin

book.routes.js:	statistics and book short,search,filter resources

user.routes.js: GET public & protected resources


– middlewares

verifySignUp.js: check duplicate Username or Email

authJwt.js: verify Token, check User roles in database


– controllers

auth.controller.js: handle signup & signin actions

book.controller.js: handle books CRUD operation

payment.controller.js: handle payments CRUD operation

record.controller.js: handle records CRUD operation

ssfbook.controller.js: handle book search,filter,sort functions

ssfuser.controller.js: handle users search,sort,filter functions

statistics.controller.js: handle statistics operations

user.controller.js: return public & protected content


– models for Sequelize Models

book.model.js

payment.model.js

record.model.js

user.model.js

role.model.js

– server.js: import and initialize neccesary modules and routes, listen for connections.


Let me explain

– import express, body-parser and cors modules:

Express is for building the Rest apis

body-parser helps to parse the request and create the req.body object

cors provides Express middleware to enable CORS

– create an Express app, then add body-parser and cors middlewares using app.use() method. Notice that we set origin: http://localhost:8081.

– define a GET route which is simple for test.

– listen on port 8090 for incoming requests.

Now let’s run the app with command: node server.js.

Open a browser with url http://localhost:8090/,  will see:

{ message: "Welcome to Library Management System application." }


Controller for Authentication

There are 2 main functions for Authentication:

- signup: create new User in database (role is user if not specifying role)

- signin:

find username of the request in database, if it exists

compare password with password in database using bcrypt, if it is correct

generate a token using jsonwebtoken

return user information & access Token





**Project setup**

npm install

Then, edit app/config/db.config.js with correct DB credentials.

**Run**

node server.js

**Test the APIs**

Using postman,

**1.signup new account POST /api/auth/signup**	

![](sup.png)

output:		"User registered successfully!"

**2.login an account POST	/api/auth/signin**	

![](sin.png)
![](sin1.png)

**3.Create a book POST  /api/test/librarian/books**

![](book.png)

**4.  Create a record POST   /api/test/librarian/records**	

bookId and booked must be same.

one bookid only one user can register
![](rec1.png)

![](rec.png)

**5.   Create a payment  POST	  /api/test/librarian/payments**	

![](pay.png)

**6.  Find oldest book in details GET	 /oldestbook**		  

![](old.png)
